﻿using UnityEngine;
using System.Collections;

public class BouncerScript : MonoBehaviour {

    public float pushFactor = 5000;

	void OnTriggerEnter( Collider other)
    {
        if ( other.tag == "Ball")
        {
            Debug.Log("bounce!");
            Vector3 pushVector = other.transform.position - gameObject.transform.position;
            pushVector.Normalize();
            other.rigidbody.AddForce(pushVector * pushFactor);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class DeathzoneScript : MonoBehaviour {


    // objektin tuhoutuminen
    void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;

public class PinballDeviceController : MonoBehaviour {

    public Transform leftFlipper;
    public Transform rightFlipper;
    public Transform ballPrefab;
    public Transform launcher;

    public Transform spawnPoint;


    public float force = 100.0f;

    void FixedUpdate()
    {
        if ( Input.GetKeyDown(KeyCode.LeftArrow))
        {
            leftFlipper.rigidbody.AddTorque(Vector3.up * -force);
        }

        if ( Input.GetKeyUp(KeyCode.LeftArrow))
        {
            leftFlipper.rigidbody.AddTorque(Vector3.up * force);
        }
       
    
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            rightFlipper.rigidbody.AddTorque(Vector3.up * force);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            rightFlipper.rigidbody.AddTorque(Vector3.up * -force);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            launcher.rigidbody.AddForce(Vector2.up * force);
        }

        if ( Input.GetAxis("PallonLisäys") == 1.0f)
        {
            Instantiate(ballPrefab, spawnPoint.position, Quaternion.identity);
        }
    }
    
       
   

    
}
